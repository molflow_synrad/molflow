
#define XMODE 1
#define YMODE 2
#define ZMODE 3
#define FACETUMODE 4
#define FACETVMODE 5
#define FACETNMODE 6
#define TWOVERTEXMODE 7
#define EQMODE 8

#include "Movement.h"
#include "GLAppGui/GLTitledPanel.h"
#include "GLAppGui/GLToolkit.h"
#include "GLAppGui/GLMessageBox.h"
#include "GLAppGui/GLButton.h"
#include "GLAppGui/GLTextField.h"
#include "GLAppGui/GLLabel.h"
#include "GLAppGui/GLRadioButton.h"
#include "GLAppGui/GLRadioGroup.h"
#include "GLAppCore/GLCoreTypes.h"
#include "Geometry_shared.h"
#include "MolFlow.h"
#include <sstream>

extern MolFlow *mApp;

/**
* \brief Constructor with basic initialisation for the movement window (Tools/moving parts)
* \param g pointer to InterfaceGeometry
* \param w pointer to Worker handler
*/
Movement::Movement(InterfaceGeometry *g,Worker *w):GLWindow() {

	int wD = 537;
	int hD = 312;

	infoLabel = new GLLabel("Movement parameters set here will only apply\nto facets which are marked \"moving\" in their parameters");
	infoLabel->SetBounds(12, 9, 261, 26);
	Add(infoLabel);GLTitledPanel* groupBox1 = new GLTitledPanel("Motion type");
	groupBox1->SetBounds(15, 49, 495, 206);
	Add(groupBox1);

	motionTypeRadioGroup = std::make_shared<GLRadioGroup>();

	radioButton_noMove = new GLRadioButton(0, "No moving parts", motionTypeRadioGroup);
	groupBox1->SetCompBounds(radioButton_noMove, 6, 19, 103, 17);
	groupBox1->Add(radioButton_noMove);

	radioButton_fixed = new GLRadioButton(0, "Fixed (same velocity vector everywhere)", motionTypeRadioGroup);
	groupBox1->SetCompBounds(radioButton_fixed, 6, 43, 215, 17);
	groupBox1->Add(radioButton_fixed);

	vzLabel = new GLLabel("vz");
	groupBox1->SetCompBounds(vzLabel, 284, 62, 18, 13);
	groupBox1->Add(vzLabel);

	vyLabel = new GLLabel("vy");
	groupBox1->SetCompBounds(vyLabel, 201, 63, 18, 13);
	groupBox1->Add(vyLabel);

	vyText = new GLTextField(0, "0");
	groupBox1->SetCompBounds(vyText, 238, 60, 40, 20);
	groupBox1->Add(vyText);

	vzText = new GLTextField(0, "0");
	groupBox1->SetCompBounds(vzText, 308, 60, 40, 20);
	groupBox1->Add(vzText);

	vxLabel = new GLLabel("vx");
	groupBox1->SetCompBounds(vxLabel, 135, 63, 18, 13);
	groupBox1->Add(vxLabel);

	vxText = new GLTextField(0, "0");
	groupBox1->SetCompBounds(vxText, 156, 60, 40, 20);
	groupBox1->Add(vxText);

	velocityVectorLabel = new GLLabel("Velocity vector [m/s]:");
	groupBox1->SetCompBounds(velocityVectorLabel, 22, 63, 107, 13);
	groupBox1->Add(velocityVectorLabel);

	radioButton_rotation = new GLRadioButton(0, "Rotation around axis", motionTypeRadioGroup);
	groupBox1->SetCompBounds(radioButton_rotation, 6, 86, 123, 17);
	groupBox1->Add(radioButton_rotation);

	hzLabel = new GLLabel("Hz");
	groupBox1->SetCompBounds(hzLabel, 284, 165, 20, 13);
	groupBox1->Add(hzLabel);

	degSLabel = new GLLabel("deg/s");
	groupBox1->SetCompBounds(degSLabel, 201, 165, 35, 13);
	groupBox1->Add(degSLabel);

	rpmLabel = new GLLabel("RPM");
	groupBox1->SetCompBounds(rpmLabel, 122, 165, 31, 13);
	groupBox1->Add(rpmLabel);

	hzText = new GLTextField(0, "0");
	groupBox1->SetCompBounds(hzText, 308, 161, 40, 20);
	groupBox1->Add(hzText);

	degText = new GLTextField(0, "0");
	groupBox1->SetCompBounds(degText, 238, 161, 40, 20);
	groupBox1->Add(degText);

	rpmText = new GLTextField(0, "0");
	groupBox1->SetCompBounds(rpmText, 156, 161, 40, 20);
	groupBox1->Add(rpmText);

	rotSpeedLabel = new GLLabel("Rotation speed:");
	groupBox1->SetCompBounds(rotSpeedLabel, 22, 165, 82, 13);
	groupBox1->Add(rotSpeedLabel);

	targetVertexButton = new GLButton(0, "Base to sel. vertex");
	groupBox1->SetCompBounds(targetVertexButton, 354, 131, 123, 23);
	groupBox1->Add(targetVertexButton);

	rzLabel = new GLLabel("rz");
	groupBox1->SetCompBounds(rzLabel, 284, 135, 15, 13);
	groupBox1->Add(rzLabel);

	ryLabel = new GLLabel("ry");
	groupBox1->SetCompBounds(ryLabel, 201, 136, 15, 13);
	groupBox1->Add(ryLabel);

	ryText = new GLTextField(0, "0");
	groupBox1->SetCompBounds(ryText, 238, 132, 40, 20);
	groupBox1->Add(ryText);

	rzText = new GLTextField(0, "0");
	groupBox1->SetCompBounds(rzText, 308, 133, 40, 20);
	groupBox1->Add(rzText);

	rxLabel = new GLLabel("rx");
	groupBox1->SetCompBounds(rxLabel, 135, 136, 15, 13);
	groupBox1->Add(rxLabel);

	rxText = new GLTextField(0, "0");
	groupBox1->SetCompBounds(rxText, 156, 132, 40, 20);
	groupBox1->Add(rxText);

	axisDirLabel = new GLLabel("Axis direction:");
	groupBox1->SetCompBounds(axisDirLabel, 22, 136, 72, 13);
	groupBox1->Add(axisDirLabel);

	baseVertexButton = new GLButton(0, "Use selected vertex");
	groupBox1->SetCompBounds(baseVertexButton, 354, 101, 123, 23);
	groupBox1->Add(baseVertexButton);

	azLabel = new GLLabel("az");
	groupBox1->SetCompBounds(azLabel, 284, 106, 18, 13);
	groupBox1->Add(azLabel);

	ayLabel = new GLLabel("ay");
	groupBox1->SetCompBounds(ayLabel, 201, 106, 18, 13);
	groupBox1->Add(ayLabel);

	ayText = new GLTextField(0, "0");
	groupBox1->SetCompBounds(ayText, 238, 102, 40, 20);
	groupBox1->Add(ayText);

	azText = new GLTextField(0, "0");
	groupBox1->SetCompBounds(azText, 308, 103, 40, 20);
	groupBox1->Add(azText);

	axLabel = new GLLabel("ax");
	groupBox1->SetCompBounds(axLabel, 135, 106, 18, 13);
	groupBox1->Add(axLabel);

	axText = new GLTextField(0, "0");
	groupBox1->SetCompBounds(axText, 156, 102, 40, 20);
	groupBox1->Add(axText);

	axisBasePointLabel = new GLLabel("Axis base point:");
	groupBox1->SetCompBounds(axisBasePointLabel, 22, 106, 81, 13);
	groupBox1->Add(axisBasePointLabel);

	applyButton = new GLButton(0, "Apply");
	applyButton->SetBounds(176, 261, 75, 23);
	Add(applyButton);

	fixed_velocity_textboxes = { vxText, vyText, vzText };
	rotation_textboxes = { axText, ayText, azText,
		rxText, ryText, rzText,
		rpmText, degText, hzText };

	//Choose no movement by default
	OnRadioUpdate(radioButton_noMove);

	SetTitle("Define moving parts");
	// Center dialog
	int wS, hS;
	GLToolkit::GetWindowSize(&wS, &hS);
	int xD = (wS - wD) / 2;
	int yD = (hS - hD) / 2;
	SetBounds(xD, yD, wD, hD);

	interfGeom = g;
	work = w;

}

/**
* \brief Function for processing various inputs (button, radio buttons etc.)
* \param src Exact source of the call
* \param message Type of the source (button)
*/
void Movement::ProcessMessage(GLComponent *src,int message) {

	switch(message) {
	case MSG_RADIO:
		OnRadioUpdate(src);
		break;

	case MSG_BUTTON:

		if (src==applyButton) { //Apply
			double a, b, c, u, v, w;
			Vector3d AXIS_P0, AXIS_DIR;
			double degPerSec;
			
			switch (mode) {

			case MovementType::Fixed:
				if (!(vxText->GetNumber(&a))) {
					GLMessageBox::Display("Invalid vx coordinate", "Error", GLDLG_OK, GLDLG_ICONERROR);
					return;
				}
				if (!(vyText->GetNumber(&b))) {
					GLMessageBox::Display("Invalid vy coordinate", "Error", GLDLG_OK, GLDLG_ICONERROR);
					return;
				}
				if (!(vzText->GetNumber(&c))) {
					GLMessageBox::Display("Invalid vz coordinate", "Error", GLDLG_OK, GLDLG_ICONERROR);
					return;
				}
				AXIS_DIR.x = a; AXIS_DIR.y = b; AXIS_DIR.z = c;
				break;
			
			case MovementType::Rotation:
				
				if (!(axText->GetNumber(&a))) {
					GLMessageBox::Display("Invalid ax coordinate", "Error", GLDLG_OK, GLDLG_ICONERROR);
					return;
				}
				if (!(ayText->GetNumber(&b))) {
					GLMessageBox::Display("Invalid ay coordinate", "Error", GLDLG_OK, GLDLG_ICONERROR);
					return;
				}
				if (!(azText->GetNumber(&c))) {
					GLMessageBox::Display("Invalid az coordinate", "Error", GLDLG_OK, GLDLG_ICONERROR);
					return;
				}
				if (!(rxText->GetNumber(&u))) {
					GLMessageBox::Display("Invalid rx coordinate", "Error", GLDLG_OK, GLDLG_ICONERROR);
					return;
				}
				if (!(ryText->GetNumber(&v))) {
					GLMessageBox::Display("Invalid ry coordinate", "Error", GLDLG_OK, GLDLG_ICONERROR);
					return;
				}
				if (!(rzText->GetNumber(&w))) {
					GLMessageBox::Display("Invalid rz coordinate", "Error", GLDLG_OK, GLDLG_ICONERROR);
					return;
				}
				if (!(degText->GetNumber(&degPerSec))) {
					GLMessageBox::Display("Invalid rotation speed (deg/s field)", "Error", GLDLG_OK, GLDLG_ICONERROR);
					return;
				}

				AXIS_P0 = Vector3d(a,b,c);
				AXIS_DIR = Vector3d (u,v,w);

				if (AXIS_DIR.Norme() < 1E-5) {
					GLMessageBox::Display("The rotation vector is shorter than 1E-5 cm.\n"
						"Very likely this is a null vector\n"
						"If not, increase its coefficients while keeping its direction", "Error", GLDLG_OK, GLDLG_ICONERROR);
					return;
				}

				break;
			}
			
			//Validation passed, apply

			if (mApp->AskToReset()) {
				work->model->sp.motionType = mode;
				switch (mode) {
				case MovementType::Fixed:
					work->model->sp.motionVector2 = AXIS_DIR;
					break;
				case MovementType::Rotation: 
					work->model->sp.motionVector1 = AXIS_P0;
					work->model->sp.motionVector2 = AXIS_DIR.Normalized() * (degPerSec / 180.0 * 3.14159);
					break;
				}

				work->MarkToReload(); 
				mApp->UpdateFacetlistSelected();
				mApp->UpdateViewers();
				mApp->changedSinceSave = true;
				return;
			}
		}
		else if (src == baseVertexButton) { //Use selected vertex as base
			size_t nbs = interfGeom->GetNbSelectedVertex();
			if (nbs != 1) {
				std::ostringstream strstr;
				strstr << "Exactly one vertex needs to be selected.\n(You have selected " << nbs << ".)";
				GLMessageBox::Display(strstr.str().c_str(), "Can't use vertex as base", GLDLG_OK, GLDLG_ICONWARNING);
				return;
			}
			else {
				motionTypeRadioGroup->Select(radioButton_rotation);
				OnRadioUpdate(radioButton_rotation); //change textbox editability
				for (int i = 0; i < interfGeom->GetNbVertex(); i++) {
					if (interfGeom->GetVertex(i)->selected) {
						axText->SetText(interfGeom->GetVertex(i)->x);
						ayText->SetText(interfGeom->GetVertex(i)->y);
						azText->SetText(interfGeom->GetVertex(i)->z);
						break;
					}
				}
			}
		}
		else if (src == targetVertexButton) {
			size_t nbs = interfGeom->GetNbSelectedVertex();
			if (nbs != 1) {
				std::ostringstream strstr;
				strstr << "Exactly one vertex needs to be selected.\n(You have selected " << nbs << ".)";
				GLMessageBox::Display(strstr.str().c_str(), "Can't use vertex as direction", GLDLG_OK, GLDLG_ICONWARNING);
				return;
			}
			else {
				motionTypeRadioGroup->Select(radioButton_rotation);
				OnRadioUpdate(radioButton_rotation); //change textbox editability
				double ax, ay, az;
				if (!axText->GetNumber(&ax)) {
					GLMessageBox::Display("Wrong ax value", "Can't use vertex as direction", GLDLG_OK, GLDLG_ICONWARNING);
					return;
				}
				if (!ayText->GetNumber(&ay)) {
					GLMessageBox::Display("Wrong ay value", "Can't use vertex as direction", GLDLG_OK, GLDLG_ICONWARNING);
					return;
				}
				if (!azText->GetNumber(&az)) {
					GLMessageBox::Display("Wrong az value", "Can't use vertex as direction", GLDLG_OK, GLDLG_ICONWARNING);
					return;
				}
				for (int i = 0; i < interfGeom->GetNbVertex(); i++) {
					if (interfGeom->GetVertex(i)->selected) {
						rxText->SetText(interfGeom->GetVertex(i)->x-ax);
						ryText->SetText(interfGeom->GetVertex(i)->y-ay);
						rzText->SetText(interfGeom->GetVertex(i)->z-az);
						break;
					}
				}
			}
		}
		break;
	case MSG_TEXT_UPD:
		if (src == rpmText || src == degText || src == hzText) {
			double num;
			if (((GLTextField*)src)->GetNumber(&num)) { //User entered an interpretable number
				if (src == rpmText) {
					degText->SetText(num * 6);
					hzText->SetText(num / 60);
				}
				else if (src == degText) {
					rpmText->SetText(num / 6);
					hzText->SetText(num / 360);
				}
				else if (src == hzText) {
					rpmText->SetText(num * 60);
					degText->SetText(num * 360);
				}
			}
		}
		break;
	}
	GLWindow::ProcessMessage(src,message);
}

/**
* \brief Updates values in the GUI for the Movements window
*/
void Movement::Update() {
	
	mode = work->model->sp.motionType;

	switch (work->model->sp.motionType) {
		case MovementType::None:
			motionTypeRadioGroup->Select(radioButton_noMove);
			OnRadioUpdate(radioButton_noMove);
			for (auto& textBox : fixed_velocity_textboxes) {
				textBox->SetText("0");
			}
			for (auto& textBox : rotation_textboxes) {
				textBox->SetText("0");
			}
		break;
		case MovementType::Fixed:
			motionTypeRadioGroup->Select(radioButton_fixed);
			OnRadioUpdate(radioButton_noMove);
			for (auto& textBox : rotation_textboxes) {
				textBox->SetText("0");
			}
			vxText->SetText(work->model->sp.motionVector2.x);
			vyText->SetText(work->model->sp.motionVector2.y);
			vzText->SetText(work->model->sp.motionVector2.z);
		break;
		case MovementType::Rotation: {
			motionTypeRadioGroup->Select(radioButton_rotation);
			OnRadioUpdate(radioButton_rotation);
			for (auto& textBox : fixed_velocity_textboxes) {
				textBox->SetText("0");
			}
			axText->SetText(work->model->sp.motionVector1.x);
			ayText->SetText(work->model->sp.motionVector1.y);
			azText->SetText(work->model->sp.motionVector1.z);
			Vector3d rot = work->model->sp.motionVector2.Normalized();
			rxText->SetText(rot.x);
			ryText->SetText(rot.y);
			rzText->SetText(rot.z);
			double num=work->model->sp.motionVector2.Norme()/3.14159*180.0;
			degText->SetText(num);
			rpmText->SetText(num / 6);
			hzText->SetText(num / 360);
		break; }
	}
}

void Movement::OnRadioUpdate(GLComponent *src) {

	if (src == radioButton_noMove) {
		mode = MovementType::None;
	}
	else if (src == radioButton_fixed){
		mode = MovementType::Fixed;
	}
	else if (src == radioButton_rotation) {
		mode = MovementType::Rotation;
	}
	else {
		throw Error("Unknown movement mode");
	}

	for (auto& textBox : fixed_velocity_textboxes) {
		textBox->SetEditable(src == radioButton_fixed);
	}

	for (auto& textBox : rotation_textboxes) {
		textBox->SetEditable(src == radioButton_rotation);
	}
}