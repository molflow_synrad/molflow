/*
  File:        TestCube.h
  Description: Test cube creator and plotter
*/

#include "GLAppGui/GLWindow.h"
#include "GLAppGui/GLButton.h"
#include "GLAppGui/GLTextField.h"
#include "GLAppGui/GLLabel.h"
#include "GLAppGui/GLTitledPanel.h"
#include "GLAppGui/GLCombo.h"

#include "Geometry.h"
#include "Worker.h"

#ifndef _TESTCUBEH_
#define _TESTCUBEH_

class TestCube : public GLWindow {

public:

  // Construction
  TestCube(InterfaceGeometry *interfGeom,Worker *work);

  // Implementation
  void ProcessMessage(GLComponent *src,int message) override;

private:

  InterfaceGeometry     *interfGeom;
  Worker	   *work;

  GLButton    *buildButton;
  GLButton    *deleteButton;
  
  GLTextField *fromStr,*toStr;
  GLTextField *posX,*posY,*posZ;
  GLTextField *size;

  GLCombo structureId;

  GLTextField* value1,value2;

  int nbFacetS;

};

#endif /* _TESTCUBEH_ */
