#include "TexturePlotter.h"
#include "GLAppGui/GLToolkit.h"
#include "GLAppGui/GLMessageBox.h"
#include "portable-file-dialogs.h"
#include "Simulation/Simulation_shared.h"
#include "GLAppGui/GLLabel.h"
#include "GLAppGui/GLButton.h"
#include "GLAppGui/GLList.h"
#include "GLAppGui/GLCombo.h"
#include "GLAppGui/GLToggle.h"
#include "Geometry_shared.h"
#include "Facet_shared.h"
#include <fstream>

#if defined(MOLFLOW)
#include "../../src/MolFlow.h"
extern MolFlow *mApp;
#endif

#if defined(SYNRAD)
#include "../src/SynRad.h"
extern SynRad*mApp;
#endif

extern std::vector<std::string> fileExportDataFilters;

/**
* \brief Constructor with initialisation for Texture plotter window (Tools/Texture Plotter)
*/
TexturePlotter::TexturePlotter() :GLWindow() {

	int wD = 500;
	int hD = 300;
	lastUpdate = 0.0f;

	SetTitle("Texture plotter");
	SetResizable(true);
	SetIconfiable(true);
	SetMinimumSize(wD, hD);

	mapList = new GLList(0);
	mapList->SetColumnLabelVisible(true);
	mapList->SetRowLabelVisible(true);
	mapList->SetAutoColumnLabel(true);
	mapList->SetAutoRowLabel(true);
	mapList->SetRowLabelMargin(20);
	mapList->SetGrid(true);
	mapList->SetSelectionMode(BOX_CELL);
	mapList->SetCornerLabel("\202\\\201");
	Add(mapList);

	viewLabel = new GLLabel("View:");
	Add(viewLabel);
	viewCombo = new GLCombo(0);
	viewCombo->SetSize(9);
	viewCombo->SetValueAt(0, "Cell area (cm\262)");
	viewCombo->SetValueAt(1, "# of MC hits");
	viewCombo->SetValueAt(2, "Impingement rate [1/m\262/sec]");
	viewCombo->SetValueAt(3, "Particle density [1/m3]");
	viewCombo->SetValueAt(4, "Gas density [kg/m3]");
	viewCombo->SetValueAt(5, "Pressure [mbar]");
	viewCombo->SetValueAt(6, "Avg.speed estimate[m/s]");
	viewCombo->SetValueAt(7, "Incident velocity vector[m/s]");
	viewCombo->SetValueAt(8, "# of velocity vectors");

	viewCombo->SetSelectedIndex(5); //Pressure by default
	Add(viewCombo);

	saveButton = new GLButton(0, "Save");
	Add(saveButton);

	sizeButton = new GLButton(0, "Autosize");
	Add(sizeButton);

	maxButton = new GLButton(0, "Find Max.");
	Add(maxButton);

	autoSizeOnUpdate = new GLToggle(0, "Autosize on every update (disable for smooth scrolling)");
	autoSizeOnUpdate->SetState(true);
	Add(autoSizeOnUpdate);

	// Center dialog
	int wS, hS;
	GLToolkit::GetWindowSize(&wS, &hS);
	int xD = (wS - wD) / 2;
	int yD = (hS - hD) / 2;
	SetBounds(xD, yD, wD, hD);

	RestoreDeviceObjects();

	worker = NULL;

}

/**
* \brief Places all components (buttons, text etc.) at the right position inside the window
*/
void TexturePlotter::PlaceComponents() {

	mapList->SetBounds(5, 5, _width - 15, _height - 80);
	saveButton->SetBounds(10, _height - 70, 70, 19);
	sizeButton->SetBounds(10, _height - 45, 70, 19);
	autoSizeOnUpdate->SetBounds(90, _height - 45, 120, 19);
	maxButton->SetBounds(90, _height - 70, 70, 19);
	viewLabel->SetBounds(320, _height - 70, 30, 19);
	viewCombo->SetBounds(350, _height - 70, 130, 19);

}

/**
* \brief Sets positions and sizes of the window
* \param x x-coordinate of the element
* \param y y-coordinate of the element
* \param w width of the element
* \param h height of the element
*/
void TexturePlotter::SetBounds(int x, int y, int w, int h) {

	GLWindow::SetBounds(x, y, w, h);
	PlaceComponents();

}


/**
* \brief Sets selected facet ID in the title of the window
*/
void TexturePlotter::GetSelected() {

	if (!worker) return;

	InterfaceGeometry *interfGeom = worker->GetGeometry();
	selFacetId = -1;
	selFacet = NULL;
	int i = 0;
	size_t nb = interfGeom->GetNbFacet();
	while (selFacetId==-1 && i < nb) {
		if (interfGeom->GetFacet(i)->selected) {
			selFacetId = i;
			selFacet = interfGeom->GetFacet(i);
		}
		else {
			i++;
		}
	}

	char tmp[64];
	sprintf(tmp, "Texture plotter [Facet #%d]", i + 1);
	SetTitle(tmp);

}

/**
* \brief Updates table values if necessary
* \param appTime curent time of the application
* \param force if update should be forced
*/
void TexturePlotter::Update(float appTime, bool force) {

	if (!IsVisible()) return;

	if (force) {
		UpdateTable();
		lastUpdate = appTime;
		return;
	}

}

/**
* \brief Update table values for selected facets (only shows facet with lowest ID) corresponding to the texture values
* \param appTime curent time of the application
* \param force if update should be forced
*/
void TexturePlotter::UpdateTable() {
	size_t nbMoments = mApp->worker.interfaceMomentCache.size();
	size_t facetHitsSize = (1 + nbMoments) * sizeof(FacetHitBuffer);
	maxValue = 0.0f;
	//double scale;
	GetSelected();
	if (!selFacet || selFacet->cellPropertiesIds.empty()) {
		mapList->Clear();
		return;
	}

	//SHELEM *mesh = selFacet->mesh;
	if (!selFacet->cellPropertiesIds.empty()) {

		size_t w = selFacet->sh.texWidth;
		size_t h = selFacet->sh.texHeight;
		mapList->SetSize(w, h);
		mapList->SetAllColumnAlign(ALIGN_CENTER);

		int mode = viewCombo->GetSelectedIndex();
		if (!worker->ReloadIfNeeded()) return;
		auto lock = GetHitLock(worker->globalState.get(), 10000);
		if (!lock) return;

		switch (mode) {

		case 0: {// Cell area
			for (size_t i = 0; i < w; i++) {
				for (size_t j = 0; j < h; j++) {
				    FacetMomentSnapshot* dummPointer = nullptr;
					double val = worker->GetGeometry()->GetPhysicalValue_textureCell(selFacet, PhysicalMode::CellArea, 1.0, 1.0,  (int)(i + j*w), *dummPointer).value;
					if (val > maxValue) {
						maxValue = val;
						maxX = i; maxY = j;
					}
					mapList->SetValueAt(i, j, fmt::format("{:g}", val));
				}
			}
			break; }

		case 1: {// MC Hits

			size_t profSize = (selFacet->sh.isProfile) ? (PROFILE_SIZE * sizeof(ProfileSlice)*(1 + nbMoments)) : 0;
			const auto& facetSnapshot = worker->globalState->facetStates[selFacetId].momentResults[mApp->worker.displayedMoment];
			for (size_t i = 0; i < w; i++) {
				for (size_t j = 0; j < h; j++) {
					PhysicalValue val = worker->GetGeometry()->GetPhysicalValue_textureCell(selFacet, PhysicalMode::MCHits, 1.0, 1.0,  (int)(i + j*w), facetSnapshot);
					double realVal = val.value;
					if (realVal > maxValue) {
						maxValue = realVal;
						maxX = i; maxY = j;
					}
					mapList->SetValueAt(i, j, fmt::format("{:g}", realVal));
				}
			}
			break; }

		case 2: {// Impingement rate

				size_t profSize = (selFacet->sh.isProfile) ? (PROFILE_SIZE * sizeof(ProfileSlice)*(1 + nbMoments)) : 0;
				const auto& facetSnapshot = worker->globalState->facetStates[selFacetId].momentResults[mApp->worker.displayedMoment];
				double moleculesPerTP = mApp->worker.Get_MoleculesPerSecond_Per_TestParticle(worker->displayedMoment);
				for (size_t i = 0; i < w; i++) {
					for (size_t j = 0; j < h; j++) {
						PhysicalValue val = worker->GetGeometry()->GetPhysicalValue_textureCell(selFacet, PhysicalMode::ImpingementRate, moleculesPerTP, worker->model->sp.gasMass, (int)(i + j * w), facetSnapshot);
						double realVal = val.value;
						if (realVal > maxValue) {
							maxValue = realVal;
							maxX = i; maxY = j;
						}
						mapList->SetValueAt(i, j, fmt::format("{:g}", realVal));
					}
				}				
			
			break; }

		case 3: {// Particle density [1/m3]

			size_t profSize = (selFacet->sh.isProfile) ? (PROFILE_SIZE * sizeof(ProfileSlice)*(1 + nbMoments)) : 0;
			const auto& facetSnapshot = worker->globalState->facetStates[selFacetId].momentResults[mApp->worker.displayedMoment];

			double moleculesPerTP = mApp->worker.Get_MoleculesPerSecond_Per_TestParticle(worker->displayedMoment);

			for (size_t i = 0; i < w; i++) {
				for (size_t j = 0; j < h; j++) {
					PhysicalValue val = worker->GetGeometry()->GetPhysicalValue_textureCell(selFacet, PhysicalMode::ParticleDensity, moleculesPerTP, worker->model->sp.gasMass, (int)(i + j*w), facetSnapshot);
					double rho = val.value;

					if (rho > maxValue) {
						maxValue = rho;
						maxX = i; maxY = j;
					}

					mapList->SetValueAt(i, j, fmt::format("{:g}", rho));
				}
			}

			break; }

		case 4: {// Gas density [kg/m3]

			size_t profSize = (selFacet->sh.isProfile) ? (PROFILE_SIZE * sizeof(ProfileSlice)*(1 + nbMoments)) : 0;
			const auto& facetSnapshot = worker->globalState->facetStates[selFacetId].momentResults[mApp->worker.displayedMoment];

			double moleculesPerTP = mApp->worker.Get_MoleculesPerSecond_Per_TestParticle(worker->displayedMoment);

			for (size_t i = 0; i < w; i++) {
				for (size_t j = 0; j < h; j++) {
					PhysicalValue val = worker->GetGeometry()->GetPhysicalValue_textureCell(selFacet, PhysicalMode::GasDensity, moleculesPerTP, worker->model->sp.gasMass, (int)(i + j*w), facetSnapshot);
					double rho_mass = val.value;
					if (rho_mass > maxValue) {
						maxValue = rho_mass;
						maxX = i; maxY = j;
					}

					mapList->SetValueAt(i, j, fmt::format("{:g}", rho_mass));
				}
			}

			break; }

		case 5: {// Pressure
				
			size_t profSize = (selFacet->sh.isProfile) ? (PROFILE_SIZE * sizeof(ProfileSlice)*(1 + nbMoments)) : 0;
			const auto& facetSnapshot = worker->globalState->facetStates[selFacetId].momentResults[mApp->worker.displayedMoment];

			double moleculesPerTP = mApp->worker.Get_MoleculesPerSecond_Per_TestParticle(worker->displayedMoment);

			for (size_t i = 0; i < w; i++) {
				for (size_t j = 0; j < h; j++) {

					PhysicalValue val = worker->GetGeometry()->GetPhysicalValue_textureCell(selFacet, PhysicalMode::Pressure, moleculesPerTP, worker->model->sp.gasMass, (int)(i + j*w), facetSnapshot);
					double p = val.value;

					if (p > maxValue) {
						maxValue = p;
						maxX = i; maxY = j;
					}

					mapList->SetValueAt(i, j, fmt::format("{:g}", p));
				}
			}
			break; }

		case 6: {// Average gas velocity [m/s]
				
				size_t profSize = (selFacet->sh.isProfile) ? (PROFILE_SIZE * sizeof(ProfileSlice)*(1 + nbMoments)) : 0;
				const auto& facetSnapshot = worker->globalState->facetStates[selFacetId].momentResults[mApp->worker.displayedMoment];

                for (size_t i = 0; i < w; i++) {
					for (size_t j = 0; j < h; j++) {
						PhysicalValue val = worker->GetGeometry()->GetPhysicalValue_textureCell(selFacet, PhysicalMode::AvgGasVelocity, 1.0, 1.0, (int)(i + j*w), facetSnapshot);
						double realVal = val.value;

						if (realVal > maxValue) {
							maxValue = realVal;
							maxX = i; maxY = j;
						}
						mapList->SetValueAt(i, j, fmt::format("{:g}", realVal));
					}
				}
			break; }

		case 7: {// Gas velocity vector

			size_t profSize = (selFacet->sh.isProfile) ? (PROFILE_SIZE * sizeof(ProfileSlice)*(1 + nbMoments)) : 0;
			size_t nbElem = selFacet->sh.texWidth*selFacet->sh.texHeight;
			size_t tSize = nbElem * sizeof(TextureCell);
			size_t dSize = nbElem * sizeof(DirectionCell);
			const auto& facetSnapshot = worker->globalState->facetStates[selFacetId].momentResults[mApp->worker.displayedMoment];
			for (size_t i = 0; i < w; i++) {
				for (size_t j = 0; j < h; j++) {
					if (selFacet->sh.countDirection) {
						PhysicalValue val = worker->GetGeometry()->GetPhysicalValue_textureCell(selFacet, PhysicalMode::GasVelocityVector, 1.0, 1.0, (int)(i + j*w), facetSnapshot);
						Vector3d v_vect = val.vect;

						mapList->SetValueAt(i, j, fmt::format("{:g},{:g},{:g}",
							v_vect.x, v_vect.y, v_vect.z));
						double length = v_vect.Norme();
						if (length > maxValue) {
							maxValue = length;
							maxX = i; maxY = j;
						}
					}
					else {
						mapList->SetValueAt(i, j, "Direction not recorded");
					}
				}
			}
			break; }

		case 8: {// Nb of velocity vectors

			size_t profSize = (selFacet->sh.isProfile) ? (PROFILE_SIZE * sizeof(ProfileSlice)*(1 + nbMoments)) : 0;
			size_t nbElem = selFacet->sh.texWidth*selFacet->sh.texHeight;
			size_t tSize = nbElem * sizeof(TextureCell);
			size_t dSize = nbElem * sizeof(DirectionCell);
			const auto& facetSnapshot = worker->globalState->facetStates[selFacetId].momentResults[mApp->worker.displayedMoment];

				for (size_t i = 0; i < w; i++) {
					for (size_t j = 0; j < h; j++) {
						if (selFacet->sh.countDirection) {
							PhysicalValue val = worker->GetGeometry()->GetPhysicalValue_textureCell(selFacet, PhysicalMode::NbVelocityVectors, 1.0, 1.0, (int)(i + j*w), facetSnapshot);
							size_t count = val.count;

							mapList->SetValueAt(i, j, fmt::format("{}", count));
							double countEq = (double)count;
							if (countEq > maxValue) {
								maxValue = countEq;
								maxX = i; maxY = j;
							}
						}
						else {
							mapList->SetValueAt(i, j, "Direction not recorded");
						}
					}
				}	
			break; }
		}
	}
	if (autoSizeOnUpdate->GetState()) mapList->AutoSizeColumn();
}

/**
* \brief Displays the window
* \param w Worker handle
*/
void TexturePlotter::Display(Worker *w) {

	worker = w;
	UpdateTable();
	SetVisible(true);

}

/**
* \brief Closes the window
*/
void TexturePlotter::Close() {
	worker = NULL;
	if (selFacet) selFacet->UnselectElem();
	mapList->Clear();
}

/**
* \brief Saves table values to a file
*/
void TexturePlotter::SaveFile() {

	if (!selFacet) return;

	std::string fileName = pfd::save_file("Export texture","",fileExportDataFilters).result();
	if (fileName.empty()) return; //User closed the export dialog
	
	std::string separator = "\t"; //For anything but CSV
	if (FileUtils::GetExtension(fileName)=="csv") {
		separator=",";
	}

	size_t u, v, wu, wv;
	if (!mapList->GetSelectionBox(&u, &v, &wu, &wv)) {
		u = 0;
		v = 0;
		wu = mapList->GetNbRow();
		wv = mapList->GetNbColumn();
	}

	// Save tab or comma separated text
    std::ofstream file(fileName);
    
    if (!file.is_open()) {
        std::string errMsg = "Cannot open file: " + fileName;
        GLMessageBox::Display(errMsg.c_str(), "Error", GLDLG_OK, GLDLG_ICONERROR);
        return;
    }

    for (size_t i = u; i < u + wu; i++) {
        for (size_t j = v; j < v + wv; j++) {
            std::string str = mapList->GetValueAt(j,i); // Assuming mapList is a vector of vectors
            if (!str.empty()) file << str;
            if (j < v + wv - 1) file << separator;
        }
        file << "\n";
    }
}

/**
* \brief Function for processing various inputs (button, check boxes etc.)
* \param src Exact source of the call
* \param message Type of the source (button)
*/
void TexturePlotter::ProcessMessage(GLComponent *src, int message) {

	switch (message) {

	case MSG_CLOSE:
		Close();
		break;

	case MSG_BUTTON:
		if (src == sizeButton) {
			mapList->AutoSizeColumn();
		}
		else if (src == saveButton) {
			SaveFile();
		}
		else if (src == maxButton) {
			size_t u, v, wu, wv;
			mapList->SetSelectedCell(maxX, maxY);
			if (mapList->GetSelectionBox(&v, &u, &wv, &wu))
				selFacet->SelectElem(u, v, wu, wv);
		}
		break;

	case MSG_LIST:
		if (src == mapList) {
			size_t u, v, wu, wv;
			if (mapList->GetSelectionBox(&v, &u, &wv, &wu))
				selFacet->SelectElem(u, v, wu, wv);
		}
		break;

	case MSG_COMBO:
		if (src == viewCombo) {
			UpdateTable();
			maxButton->SetEnabled(true);
			//maxButton->SetEnabled(viewCombo->GetSelectedIndex()!=2);
		}
		break;

	}

	GLWindow::ProcessMessage(src, message);
}
