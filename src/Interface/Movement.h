#pragma once

#include "GLAppGui/GLWindow.h"
#include "Buffer_shared.h" //MovementType
#include <vector>
#include <memory>
class GLButton;
class GLTextField;
class GLLabel;
class GLRadioButton;
class GLRadioGroup;
class GLTitledPanel;

class InterfaceGeometry;
class Worker;

class Movement : public GLWindow {

public:
  // Construction
	Movement(InterfaceGeometry *interfGeom, Worker *work);
  void ProcessMessage(GLComponent *src,int message) override;
  void Update();

  // Implementation
private:
  
  GLLabel* infoLabel;
  GLTitledPanel* motionTypeGroupBox;
  GLLabel* rotSpeedLabel;
  GLLabel* rpmLabel;
  GLLabel* degSLabel;
  GLLabel* hzLabel;
  GLTextField	*hzText;
  GLTextField	*degText;
  GLTextField	*rpmText;
  GLButton	*targetVertexButton;
  GLLabel* axisDirLabel;
  GLLabel* rxLabel;
  GLLabel* ryLabel;
  GLLabel* rzLabel;
  GLTextField	*rxText;
  GLTextField	*ryText;
  GLTextField	*rzText;
  GLButton	*baseVertexButton;
  GLLabel	*axisBasePointLabel;
  GLLabel	*axLabel;
  GLLabel* ayLabel;
  GLLabel* azLabel;
  GLTextField	*axText;
  GLTextField	*ayText;
  GLTextField	*azText;
  std::shared_ptr<GLRadioGroup> motionTypeRadioGroup;
  GLRadioButton* radioButton_noMove;
  GLRadioButton* radioButton_fixed;
  GLRadioButton* radioButton_rotation;
  GLLabel* velocityVectorLabel;
  GLLabel* vxLabel;
  GLLabel* vyLabel;
  GLLabel* vzLabel;
  GLTextField	*vxText;
  GLTextField	*vyText;
  GLTextField	*vzText;
  GLButton	*applyButton;

  std::vector<GLTextField*> fixed_velocity_textboxes;
  std::vector<GLTextField*> rotation_textboxes;

  MovementType mode = MovementType::None;

  void OnRadioUpdate(GLComponent *src);

  InterfaceGeometry     *interfGeom;
  Worker	   *work;

};
