#pragma once

#include <Random.h>
#include <mutex>
#include "RayTracing/Ray.h"
#include "SimulationUnit.h"

struct FacetHitDetail;
class MolflowSimulationModel;
class GlobalSimuState;
struct ParticleLog;
class SimulationFacet;

enum class ThreadState :int;

/**
* \brief Namespace containing various simulation only classes and methods
 */
namespace MFSim {

	enum class ParticleEventType {
		FacetHit,
		Overtime,
		Decay,
		Scatter
	};

	/**
	* \brief Implements particle state and corresponding pre-/post-processing methods (source position, hit recording etc.)
	 */
	class ParticleTracer {
	public:
		//Shared implementation in Particle_shared.cpp

		bool UpdateMCHits(const std::shared_ptr<GlobalSimuState> globalState,
			std::string& myStatus, std::mutex& statusMutex, size_t timeout_ms);

		bool UpdateHitsAndLog(const std::shared_ptr<GlobalSimuState> globalState, const std::shared_ptr<ParticleLog> particleLog,
			ThreadState& myState, std::string& myStatus, std::mutex& statusMutex, size_t timeout_ms);

		bool UpdateLog(const std::shared_ptr<ParticleLog> globalLog,
			std::string& myStatus, std::mutex& statusMutex, size_t timeout);

		void LogHit(SimulationFacet* f);

		void RecordLeakPos();

		//Molflow-specific implementation
		void IncreaseDistanceCounters(double distanceIncrement);

		MCStepResult SimulationMCStep(size_t nbStep, size_t threadNum, size_t remainingDes);

		bool StartFromSource(Ray& ray);



		void RecordHitOnTexture(const SimulationFacet* f, const int m, const bool countHit,
			const double sum_1_per_ort_velocity_increment, const double sum_v_ort_increment);

		void ProfileFacet(const SimulationFacet* f, const int m, const bool countHit,
			const double sum_1_per_ort_velocity_increment, const double sum_v_ort_increment);

		void RecordHit(const int type);

		

		void IncreaseFacetCounter(const SimulationFacet* f, int momentIndex, const size_t hit, const size_t desorb, const size_t absorb,
			const double sum_1_per_ort_velocity_increment, const double sum_v_ort_increment,
			const Vector3d& impulse, const Vector3d& impulse_square, const Vector3d& impulse_momentum);

		void UpdateVelocity(const SimulationFacet* collidedFacet);

		

		void RecordDirectionVector(const SimulationFacet* f, int m);

		void RecordAngleMap(const SimulationFacet* collidedFacet);

		void PerformTeleport(SimulationFacet* teleportSourceFacet);

		void RegisterTransparentPass(SimulationFacet* facet);

		void RecordAbsorb(SimulationFacet* absorbFacet);

		void PerformBounce(SimulationFacet* bounceFacet);

		bool PerformScatter();

		void RecordHistograms(SimulationFacet* histogramFacet, int m);



		void Reset();

		size_t GetMemSize() const;

		Ray ray; // an object purely for the ray tracing related intersection tests
		double oriRatio; //Represented ratio of desorbed, used for low flux mode

		//Recordings for histogram
		uint64_t totalDesorbed;
		size_t nbBounces; // Number of hit (current particle) since desorption
		size_t lastMomentIndex; // Speedup for binary search
		size_t particleTracerId; //For parallel computing, each core gets a particle id. Lines and leak recording only for id=0
		double distanceTraveled;
		double generationTime; //Time it was created, constant
		//double particleTime; //Actual time, incremented after every hit. (Flight time = actual time - generation time)
		int teleportedFrom;   // We memorize where the particle came from: we can teleport back

		double velocity;
		double initialVelocity; //Used to check for reaching Brownian motion if background collisions are enabled
		double expectedDecayMoment; //for radioactive gases
		double expectedScatterPath; //for background collisions

		std::unique_ptr<GlobalSimuState> tmpState = std::make_unique<GlobalSimuState>(); //Thread-local "unadded" results, that are reset to 0 when added to global state. Pointer to break circular includes
		std::unique_ptr<ParticleLog> tmpParticleLog = std::make_unique<ParticleLog>(); //Pointer to break circular includes
		int lastHitFacetId = -1; //id of last hit facet, that ray tracer should avoid. Remembered to persist across multiple SimulationNBStep(n) calls
		bool insertNewParticleAtNextStep = true; //Remembered to persist across multiple SimulationNBStep(n) calls
		MersenneTwister randomGenerator;

		std::shared_ptr<MolflowSimulationModel> model;
		std::vector<SimulationFacet*> transparentHitBuffer; //Storing this buffer simulation-wide is cheaper than recreating it at every Intersect() call
		std::vector <FacetHitDetail> facetHitDetails; //One per SimulationFacet, hit details for intersect routine

		bool exitRequested{ false };

		Vector3d nullVector; //so we don't have to allocate and destroy for dummy uses

	private:
		int LookupMomentIndex(const double time, const size_t startIndex);

	};
}
