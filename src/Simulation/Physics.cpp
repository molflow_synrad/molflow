

#include "Physics.h"
#include "Helper/MathTools.h"

double Physics::GenerateRandomVelocity(const std::vector<IntegratedVelocityEntry>& maxwell_CDF_1K, const double sqrt_temperature, const double rndVal) {
    double v_1K = InterpolateX(rndVal, maxwell_CDF_1K, false, false, true); //Allow extrapolate
    return sqrt_temperature*v_1K;
}

double Physics::GenerateDesorptionTime(const std::vector<std::vector<IntegratedDesorptionEntry>> &IDs,
                                       const SimulationFacet *src, const double rndVal, double latestMoment) {
    if (!src->sh.outgassingParam.empty()) { //time-dependent desorption
        return InterpolateX(rndVal * IDs[src->sh.IDid].back().cumulativeDesValue, IDs[src->sh.IDid],
                            false, false, true); //allow extrapolate
    } else {
        return rndVal * latestMoment; //continous desorption between 0 and latestMoment
    }
}

/**
* \brief Updates particle direction and velocity if we are dealing with a moving facet (translated or rotated)
*/
void
Physics::TreatMovingFacet(std::shared_ptr<MolflowSimulationModel> model, const Vector3d &position, Vector3d &direction, double &velocity) {
    Vector3d localVelocityToAdd;
    if (model->sp.motionType == MovementType::Fixed) { //Translation
        localVelocityToAdd = model->sp.motionVector2; //Fixed translational vector
    } else if (model->sp.motionType == MovementType::Rotation) { //Rotation
        Vector3d distanceVector = 0.01 * (position -
                                          model->sp.motionVector1); //distance from base, with cm->m conversion, motionVector1 is rotation base point
        localVelocityToAdd = CrossProduct(model->sp.motionVector2, distanceVector); //motionVector2 is rotation axis
    }
    Vector3d oldVelocity, newVelocity;
    oldVelocity = direction * velocity;
    newVelocity = oldVelocity + localVelocityToAdd;
    direction = newVelocity.Normalized();
    velocity = newVelocity.Norme();
}

double Physics::GenerateNormalizedImpactParameter(const double rndVal) {
    //PDF: P(b) = 2b / rho^2    (if b<rho, 0 otherwise)
    //CDF: 0 below b=0, b^2/rho^2 between 0 and rho, 1 over rho
    //inversion method: b=rho*sqrt(rndVal)
    //normalize to rho
    return sqrt(rndVal);
}

std::pair<double, double> Physics::GetScatteringAngleAndVelocity(const double b_normalized, const double massRatio, const double pre_collision_velocity) {
    double com_scattering_angle = 2.0 * acos(b_normalized); //theta (center of mass frame)
    double tan_theta_lab = sin(com_scattering_angle) / (massRatio + cos(com_scattering_angle)); //COM frame to lab frame
    double theta_lab = atan(tan_theta_lab);
    //phi is a random azimuth angle
    //v1x = m1u/(m1+m2) + m2u/(m1+m2)cos(theta)
    //v1y=m2u/(m1+m2)*sin(theta)*cos(phi)
    //v1z=m2u/(m1+m2)*sin(theta)*sin(phi)
    //v1=sqrt(v1x^2+v1y^2+v1z^2)
    //After simplification:
    //v1=u*sqrt(1+2((m1m2)/(m1+m2)^2)*(cos(theta)-1))
    //Substituting massRatio r=m1/m2:
    //v1=u*sqrt(1+2(r/(r+1)^2)*(cos(theta)-1))
    double post_collision_velocity = pre_collision_velocity * sqrt(1.0+2.0*(massRatio/Square(massRatio+1.0))*(cos(com_scattering_angle)-1.0));
    return {theta_lab, post_collision_velocity};
}